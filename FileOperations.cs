﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DuplicateImagesFinder
{
    class FileOperations
    {
        internal static void RefreshPath(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static string createDateName(string incomePath, string basePath, DateTime time)
        {
            FileInfo baseFile = new FileInfo(incomePath);
            string ext = baseFile.Extension.ToLower();            

            string month = "";
            if (time.Month == 1) month = "01-January";
            if (time.Month == 2) month = "02-February";
            if (time.Month == 3) month = "03-March";
            if (time.Month == 4) month = "04-April";
            if (time.Month == 5) month = "05-May";
            if (time.Month == 6) month = "06-June";
            if (time.Month == 7) month = "07-July";
            if (time.Month == 8) month = "08-August";
            if (time.Month == 9) month = "09-September";
            if (time.Month == 10) month = "10-October";
            if (time.Month == 11) month = "11-November";
            if (time.Month == 12) month = "12-December";

            RefreshPath(basePath + "\\" + time.Year);
            RefreshPath(basePath + "\\" + time.Year + "\\" + month);
            RefreshPath(basePath + "\\" + time.Year + "\\" + month + "\\" + time.Day);

            string result = basePath + "\\" + time.Year + "\\" + month + "\\" + time.Day + "\\" + time.Hour + "-" + time.Minute + "-" + time.Second;
            while (File.Exists(result + ext))
            {
                result += "_";
            }
            return result + ext;
        }


        public static string createUnDateName(string incomePath, string savingsPath)
        {
            FileInfo baseFile = new FileInfo(incomePath);
            string ext = baseFile.Extension.ToLower();
            string name = baseFile.Name;

            RefreshPath(savingsPath + "\\" + ext);

            string result = savingsPath + "\\" + ext + "\\" + name.Substring(0, name.Length - ext.Length);
            while (File.Exists(result + ext))
            {
                result += "_";
            }
            return result + ext;
        }

        internal static void produceSizesAndDates(string[] incomeFiles, ref long[] incomeFileSizes, ref DateTime[] incomeFileDates)
        {
            int idate = 0;
            FileInfo incFile;

            foreach (string file in incomeFiles)
            {
                incFile = new FileInfo(file);
                incomeFileSizes[idate] = incFile.Length;
                incomeFileDates[idate] = FileChekers.getDate(incFile);
                
                idate++;
            }
        }
    }
}
