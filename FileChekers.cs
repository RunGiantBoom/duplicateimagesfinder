﻿using ExifLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DuplicateImagesFinder
{
    class FileChekers
    {
        public static bool checkPixelDiffence(string incomePath, string basePath)
        {

            FileInfo incomeFile = new FileInfo(incomePath);
            FileInfo baseFile = new FileInfo(basePath);

            if (incomeFile.Length != baseFile.Length)
            {
                return false;
            }
            else
            {
                using (Bitmap incomeImage = new Bitmap(incomePath), baseImage = new Bitmap(basePath))
                {
                    if (incomeImage.Size.Height != baseImage.Size.Height || incomeImage.Size.Width != baseImage.Size.Width)
                    {
                        return false;
                    }
                    else
                    {
                        int imageWidth = incomeImage.Size.Width;
                        int imageHeight = incomeImage.Size.Height;

                        for (int i = 0; i < imageWidth; i++)
                        {
                            for (int j = 0; j < imageHeight; j++)
                            {
                                if (incomeImage.GetPixel(i, j) != baseImage.GetPixel(i, j))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    return true;
                }
            }

        }
        public static bool byteImplementation(string incomePath, string basePath)
        {
            byte[] incomeBytes = File.ReadAllBytes(incomePath);
            byte[] baseBytes = File.ReadAllBytes(basePath);

            //bool result = BlockCompare(incomeBytes, baseBytes, 0, baseBytes.Length);
            //return result;

            for (int i = 0; i < incomeBytes.Length; i++)
            {
                if (incomeBytes[i] != baseBytes[i])
                {
                    return false;
                }
            }
            return true;
        }

        //unsafe static bool BlockCompare(byte[] buffer1, byte[] buffer2, int offset, int length)
        //{
        //    if (buffer1 == null || buffer2 == null || buffer1.Length < offset + length || buffer2.Length < offset + length) return false;
        //    if (buffer1 == buffer2) return true;

        //    int blockCount = length / sizeof(int);
        //    fixed (byte* pBase1 = buffer1, pBase2 = buffer2)
        //    {
        //        int* ptr1 = (int*)(pBase1 + offset);
        //        int* ptr2 = (int*)(pBase2 + offset);
        //        for (int i = 0; i < blockCount; i++)
        //            if (*ptr1++ != *ptr2++) return false;
        //    }
        //    for (int i = 0; i < length % sizeof(int); i++)
        //        if (buffer1[length - i - 1] != buffer2[length - i - 1]) return false;
        //    return true;
        //}

        internal static DateTime getDate(FileInfo incomeFile)
        {
            DateTime result;
            try
            {
                using (ExifReader reader = new ExifReader(incomeFile.FullName))
                {
                    reader.GetTagValue<DateTime>(ExifTags.DateTimeDigitized, out result);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Date error: {e.Message}");
                result = default(DateTime);
            }
            return result;
        }
    }
}
