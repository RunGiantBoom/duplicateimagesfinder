﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;
using System.Diagnostics;
using System.Threading;

namespace DuplicateImagesFinder
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            producer();


            //Проверка папок
            //Открытие лога.
            //Создание кэша
            //Заполнение списка с датами
            //Заполнение списка без дат.
            //Вызывается процессор. процессор получает адрес лога и папок из формы.
            //А так же два чекбокса. Нет, только один. Процессор создает массив стрингов адресов для базы и входящих файлов
        }
        void producer()
        {
            //Пути входящих файлов и базы. База понадобится позже, когда будем в неё файлы записывать
            string incomePath = sourceTextBox.Text;
            string basePath = mainFolderTextBox.Text;
            string videoPath = basePath + "\\" + videoTextBox.Text;
            string specialPath = basePath + "\\" + specialTextBox.Text;
            string savingsPath = savingsTextBox.Text;

            //Открывается лог
            Logger log = null;
            if (loggerCheckBox.Enabled)
                log = new Logger(logTextBox.Text);

            processor(incomePath, SubfoldersCheckBox.Checked, DeleteCheckBox.Checked, basePath, videoPath, specialPath, savingsPath, log);
        }
        
        void processor(string incomePath, bool subFoldersChecking, bool deleting, string basePath, string videoPath, string specialPath, string savingsPath, Logger log)
        {
            //Создается таймер общей работы
            Stopwatch stwatch = new Stopwatch();
            stwatch.Start();
            long stepTime = 0;

            //Если не слуществует входная директория, ничего не делаем.
            if (!Directory.Exists(incomePath)) return;

            //если одной из этих папок нет, создаем
            FileOperations.RefreshPath(basePath);
            FileOperations.RefreshPath(videoPath);
            FileOperations.RefreshPath(specialPath);
            FileOperations.RefreshPath(savingsPath);


            //Формирование списка адресов всех файлов, которые хотим добавить в базу
            string[] incomeFiles;

            SearchOption curSearch;
            if (subFoldersChecking)
            {
                log?.WriteLine("Обработка всех вложенных файлов");
                curSearch = SearchOption.AllDirectories;
            }
            else
            {
                log?.WriteLine("Обработка файлов верхней директории");
                curSearch = SearchOption.TopDirectoryOnly;
            }

            stepTime = stwatch.ElapsedMilliseconds;
            incomeFiles = Directory.GetFiles(incomePath, "*", curSearch);
            log?.WriteLine("Файлы обработаны за: " + (stwatch.ElapsedMilliseconds - stepTime) / (float)1000 + " секунд");


            long[] incomeFileSizes = new long[incomeFiles.Length];
            DateTime[] incomeFileDates = new DateTime[incomeFiles.Length];

            stepTime = stwatch.ElapsedMilliseconds;
            FileOperations.produceSizesAndDates(incomeFiles, ref incomeFileSizes, ref incomeFileDates);
            log?.WriteLine("Даты и размеры подготовлены за: " + (stwatch.ElapsedMilliseconds - stepTime) / (float)1000 + " секунд");

            int idate = 0, step = 0, prevstep = 0;
            step = 0;
            prevstep = 0;

            //Файлы с датами ОБЯЗАНЫ быть как минимум в папках с датами, поэтому нужно сформировать отдельный список датированных файлов из базы
            List<FileInfo> datedFiles = new List<FileInfo>();
            foreach (string directory in Directory.GetDirectories(basePath))
                if (directory != videoPath && directory != specialPath && directory != savingsPath) //В datedFiles нужно добавить все папки кроме особенных и видео
                    foreach (string filename in Directory.GetFiles(directory, "*", SearchOption.AllDirectories))
                        datedFiles.Add(new FileInfo(filename));

            
            //Для папки особенных и хранилки и видео свой список.
            List<FileInfo> undatedFiles = new List<FileInfo>();
            foreach (string filename in Directory.GetFiles(videoPath, "*", SearchOption.AllDirectories))
                undatedFiles.Add(new FileInfo(filename));
            foreach (string filename in Directory.GetFiles(specialPath, "*", SearchOption.AllDirectories))
                undatedFiles.Add(new FileInfo(filename));
            foreach (string filename in Directory.GetFiles(savingsPath, "*", SearchOption.AllDirectories))
                undatedFiles.Add(new FileInfo(filename));

            //Обновляем в форме количество файлов в источнике. Зачем нам отображать количество файлов в базе? Да просто так.
            FilesIncomeLabel.Text = incomeFiles.Length.ToString();
            FilesIncomeLabel.Update();

            log?.WriteLine("Вся подготовка завершена за: " + stwatch.ElapsedMilliseconds / (float)1000 + " секунд");
            //Отписываем в лог количество файлов
            log?.WriteLine("Файлов в базе: " + (datedFiles.Count + undatedFiles.Count));
/*специально забыт '?' */ log.WriteLine("Файлов в источнике: " + incomeFiles.Length);

            //Основные счетчики будущего процесса.
            int countFinded = 0;
            int countError = 0;
            int countCopied = 0;
            long TotalOpenedFileSize = 0;
            int findedInMemoryCounter = 0;
            int findedInVideoCounter = 0;
            int findedInSpecialCounter = 0;
            int findedInSavingsCounter = 0;
            int copiedInMemoryCounter = 0;
            int copiedInSavingsCounter = 0;

            //Счетчик одного файла
            Stopwatch curFileWatch = new Stopwatch();
            Stopwatch curFileBytesWatch = new Stopwatch();
            Stopwatch curFileCopyWatch = new Stopwatch();
            long bytesTime = 0;
            long allBytesTime = 0;
            long copyTime = 0;
            long allCopyTime = 0;

            //временные переменные
            bool finded;
            bool havedate;
            string findedPath;
            FileInfo incomeFile;
            long incomeFileLength;
            DateTime incomeFileDate;
            List<FileInfo> filesToCompareWith;

            for (int i = 0; i < incomeFiles.Length; i++)
            {
                curFileWatch.Restart();
                bytesTime = 0;
                copyTime = 0;

                finded = false;
                findedPath = "";

                //Обновление статуса в логе.
                log?.WriteLine((i + 1) + " Name: " + incomeFiles[i]);
                log?.Update();

                //Теперь нужно проверить. Есть ли у файла дата. Если есть, проверять по списку с датами, если нет - по списку без дат.

                if (incomeFileDates[i] != default(DateTime))
                {
                    filesToCompareWith = datedFiles;
                    havedate = true;
                }
                else
                {
                    filesToCompareWith = undatedFiles;
                    havedate = false;
                }

                foreach (FileInfo file in filesToCompareWith)
                {
                    if (incomeFileSizes[i] == file.Length)
                    {
                        curFileBytesWatch.Restart();
                        finded = FileChekers.byteImplementation(incomeFiles[i], file.FullName);
                        bytesTime = curFileBytesWatch.ElapsedMilliseconds;
                        allBytesTime += bytesTime;
                    }
                    if (finded)
                    {
                        findedPath = file.FullName;
                        if (havedate)
                            findedInMemoryCounter++;
                        else if (findedPath.Contains(videoPath))
                            findedInVideoCounter++;
                        else if (findedPath.Contains(specialPath))
                            findedInSpecialCounter++;
                        else if (findedPath.Contains(savingsPath))
                            findedInSavingsCounter++;
                        break;
                    }
                }

                if (finded)
                {
                    countFinded++;
                    log?.WriteLine("   Найдено. Идентично с файлом:");
                    log?.WriteLine("   " + findedPath);

                    if (deleting)
                    {
                        File.Delete(incomeFiles[i]);
                        Console.WriteLine("Удаляется файл: " + incomeFiles[i]);
                    }
                }
                else
                {
                    //Если файл не найден, то создаем новое название текущего файла.
                    string targetPath = null;

                    if (havedate)
                        targetPath = FileOperations.createDateName(incomeFiles[i], basePath, incomeFileDates[i]);
                    else
                        targetPath = FileOperations.createUnDateName(incomeFiles[i], savingsPath);

                    //Пытаемся скопировать изображение в базу.
                    try
                    {
                        curFileCopyWatch.Restart();

                        if (deleting)
                            File.Move(incomeFiles[i], targetPath);
                        else
                            File.Copy(incomeFiles[i], targetPath);

                        if (havedate)
                            copiedInMemoryCounter++;
                        else if (targetPath.Contains(savingsPath))
                            copiedInSavingsCounter++;
                        else
                        {
                            countError++;
                            Debug.WriteLine("File Copied not in two standart ways");
                        }
                        copyTime = curFileCopyWatch.ElapsedMilliseconds;
                        allCopyTime += copyTime;

                        countCopied++;
                        //Добавляем наш новый файл в список файлов базы.
                        filesToCompareWith.Add(new FileInfo(targetPath));

                        log?.WriteLine("   Отсутствует. Скопировано в базу на место:");
                        log?.WriteLine("   " + targetPath);
                    }
                    catch (Exception ex)
                    {
                        countError++;

                        if (deleting)
                            log?.WriteLine("   Ошибка перемещения:");
                        else
                            log?.WriteLine("   Ошибка копирования:");

                        log?.WriteLine(
                            "==========================Exception Start==========================\n" +
                            ex.ToString() +
                            "===========================Exception End===========================\n"
                        );
                    }
                }

                //Если эти рефреши будут занимать много времени, то добавить им отдельный таймер и если он будет больше 10 процентов от затрат времени на поиск и сравнение, то пропускать следующие рефреши.
                FilesProcessedLabel.Text = (i + 1).ToString();
                FilesProcessedLabel.Refresh();
                FindedBySearchLabel.Text = countFinded.ToString();
                FindedBySearchLabel.Refresh();
                FilesCopiedLabel.Text = countCopied.ToString();
                FilesCopiedLabel.Refresh();
                countErrorsLabel.Text = countError.ToString();
                countErrorsLabel.Refresh();
                TimeFromStartLabel.Text = (stwatch.ElapsedMilliseconds / 1000).ToString();
                TimeFromStartLabel.Refresh();

                FindedInMemoryLabel.Text = findedInMemoryCounter.ToString();
                FindedInMemoryLabel.Refresh();
                FindedInVideoLabel.Text = findedInVideoCounter.ToString();
                FindedInVideoLabel.Refresh();
                FindedInSpecialLabel.Text = findedInSpecialCounter.ToString();
                FindedInSpecialLabel.Refresh();
                FindedInSavingsLabel.Text = findedInSavingsCounter.ToString();
                FindedInSavingsLabel.Refresh();

                AddedInMemoryLabel.Text = copiedInMemoryCounter.ToString();
                AddedInMemoryLabel.Refresh();
                AddedInSavingsLabel.Text = copiedInSavingsCounter.ToString();
                AddedInSavingsLabel.Refresh();

                step = 100 * (i + 1) / incomeFiles.Length;
                if (step == prevstep + 1)
                {
                    progressBar1.Value = step;
                    progressBar1.Refresh();
                    prevstep = step;
                }

                log?.WriteLine("   Затрачено времени: " + curFileWatch.ElapsedMilliseconds / (float)1000 + " секунд. На сравнение байт: " + bytesTime / (float)1000);
                log?.WriteLine("   Размер файла: " + Math.Round(incomeFileSizes[i] / (double)1000) / 1000 + " Мб. Мегабайт в секунду: " + (Math.Round(incomeFileSizes[i] / (double)1000) / 1000) / (curFileWatch.ElapsedMilliseconds / (float)1000));
                log?.Update();
            }

            log?.WriteLine("Общее время обработки: " + stwatch.ElapsedMilliseconds / (float)1000 + " секунд");
            log?.WriteLine("      время копирования: " + allCopyTime / (float)1000 + " секунд");
            log?.WriteLine("      время проверки байт: " + allBytesTime / (float)1000 + " секунд");
            log?.WriteLine("Файлов в базе: " + (datedFiles.Count + undatedFiles.Count));
            log?.WriteLine("Файлов в источнике: " + incomeFiles.Length);
            log?.WriteLine("Файлов найдено: " + countFinded); //Добавить что файлы удалялись. и поправить файлы не найденные и с ошибками.
            log?.WriteLine("Файлов не найдено но и не скопировано (возникли проблемы): " + countError);

            if (deleting)
            {
                log?.WriteLine("Файлов перемещено из источника в базу: " + countCopied);
            }
            else
            {
                log?.WriteLine("Файлов скопировано из источника в базу: " + countCopied);
            }

            log?.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            logTextBox.Enabled = !logTextBox.Enabled;
        }

        private void DeleteCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FilesCopiedNameLabel.Text == "Скопировано")
                FilesCopiedNameLabel.Text = "Перемещено";
            else
                FilesCopiedNameLabel.Text = "Скопировано";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Сначала удаляем папку 10 в C:\EffectivnessCheck\To если она есть.
            if (Directory.Exists(@"C:\EffectivnessCheck\To\2017"))
            {
                Directory.Delete(@"C:\EffectivnessCheck\To\2017", true);
            }

            //Пути входящих файлов и базы. База понадобится позже, когда будем в неё файлы записывать
            string incomePath = @"C:\EffectivnessCheck\From";
            string basePath = @"C:\EffectivnessCheck\To";
            string videoPath = basePath + "\\Video";
            string specialPath = basePath + "\\Special";
            string savingsPath = basePath + "\\Savings";

            //Открывается лог
            Logger log = null;
            if (loggerCheckBox.Enabled)
                log = new Logger(@"C:\EffectivnessCheck\log.txt");

            processor(incomePath, SubfoldersCheckBox.Checked, false, basePath, videoPath, specialPath, savingsPath, log);
        }
    }
}
