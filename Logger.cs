﻿using System;
using System.IO;

namespace DuplicateImagesFinder
{
    internal class Logger
    {
        //Понадобится сохранять постоянно в какой файл я пишу.
        //И метод которым я пишу.
        //Файл открывать в конструкторе, передавая ему название будущего файла.
        StreamWriter log;
        string name;

        public Logger(string name)
        {
            log = new StreamWriter(name);
            this.name = name;
        }        
        public void WriteLine(string text)
        {
            log.WriteLine(text);
        }
        public void Close()
        {
            log.Close();
        }
        public void Update()
        {
            log.Close();
            log = new StreamWriter(this.name, true);
        }
    }
}
