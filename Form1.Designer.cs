﻿namespace DuplicateImagesFinder
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.sourceTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SubfoldersCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.FilesProcessedLabel = new System.Windows.Forms.Label();
            this.FilesIncomeLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.FilesLoadedLabel = new System.Windows.Forms.Label();
            this.DeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.specialTextBox = new System.Windows.Forms.TextBox();
            this.videoTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.savingsTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.FilesCopiedNameLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.FindedBySearchLabel = new System.Windows.Forms.Label();
            this.FilesCopiedLabel = new System.Windows.Forms.Label();
            this.countErrorsLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.TimeFromStartLabel = new System.Windows.Forms.Label();
            this.loggerCheckBox = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.mainFolderTextBox = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label20 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.FindedInMemoryLabel = new System.Windows.Forms.Label();
            this.FindedInVideoLabel = new System.Windows.Forms.Label();
            this.FindedInSpecialLabel = new System.Windows.Forms.Label();
            this.FindedInSavingsLabel = new System.Windows.Forms.Label();
            this.AddedInMemoryLabel = new System.Windows.Forms.Label();
            this.AddedInSavingsLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sourceTextBox
            // 
            this.sourceTextBox.Location = new System.Drawing.Point(73, 15);
            this.sourceTextBox.Name = "sourceTextBox";
            this.sourceTextBox.Size = new System.Drawing.Size(229, 20);
            this.sourceTextBox.TabIndex = 0;
            this.sourceTextBox.Text = "C:\\К сортировке";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(676, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Запуск";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Откуда";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(305, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Файлов в источнике";
            // 
            // SubfoldersCheckBox
            // 
            this.SubfoldersCheckBox.AutoSize = true;
            this.SubfoldersCheckBox.Checked = true;
            this.SubfoldersCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SubfoldersCheckBox.Location = new System.Drawing.Point(308, 17);
            this.SubfoldersCheckBox.Name = "SubfoldersCheckBox";
            this.SubfoldersCheckBox.Size = new System.Drawing.Size(121, 17);
            this.SubfoldersCheckBox.TabIndex = 7;
            this.SubfoldersCheckBox.Text = "Включая подпапки";
            this.SubfoldersCheckBox.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(306, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Обработано:";
            // 
            // FilesProcessedLabel
            // 
            this.FilesProcessedLabel.AutoSize = true;
            this.FilesProcessedLabel.Location = new System.Drawing.Point(430, 74);
            this.FilesProcessedLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FilesProcessedLabel.Name = "FilesProcessedLabel";
            this.FilesProcessedLabel.Size = new System.Drawing.Size(13, 13);
            this.FilesProcessedLabel.TabIndex = 9;
            this.FilesProcessedLabel.Text = "0";
            // 
            // FilesIncomeLabel
            // 
            this.FilesIncomeLabel.AutoSize = true;
            this.FilesIncomeLabel.Location = new System.Drawing.Point(430, 52);
            this.FilesIncomeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FilesIncomeLabel.Name = "FilesIncomeLabel";
            this.FilesIncomeLabel.Size = new System.Drawing.Size(13, 13);
            this.FilesIncomeLabel.TabIndex = 10;
            this.FilesIncomeLabel.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(472, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Количество загрузок файлов:";
            // 
            // FilesLoadedLabel
            // 
            this.FilesLoadedLabel.AutoSize = true;
            this.FilesLoadedLabel.Location = new System.Drawing.Point(649, 124);
            this.FilesLoadedLabel.Name = "FilesLoadedLabel";
            this.FilesLoadedLabel.Size = new System.Drawing.Size(13, 13);
            this.FilesLoadedLabel.TabIndex = 14;
            this.FilesLoadedLabel.Text = "0";
            // 
            // DeleteCheckBox
            // 
            this.DeleteCheckBox.AutoSize = true;
            this.DeleteCheckBox.Location = new System.Drawing.Point(435, 17);
            this.DeleteCheckBox.Name = "DeleteCheckBox";
            this.DeleteCheckBox.Size = new System.Drawing.Size(235, 17);
            this.DeleteCheckBox.TabIndex = 15;
            this.DeleteCheckBox.Text = "Удалять изображения найденные в базе";
            this.DeleteCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DeleteCheckBox.UseVisualStyleBackColor = true;
            this.DeleteCheckBox.CheckedChanged += new System.EventHandler(this.DeleteCheckBox_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Особенные";
            // 
            // specialTextBox
            // 
            this.specialTextBox.Location = new System.Drawing.Point(159, 67);
            this.specialTextBox.Name = "specialTextBox";
            this.specialTextBox.Size = new System.Drawing.Size(120, 20);
            this.specialTextBox.TabIndex = 17;
            this.specialTextBox.Text = "Особенные";
            // 
            // videoTextBox
            // 
            this.videoTextBox.Location = new System.Drawing.Point(159, 41);
            this.videoTextBox.Name = "videoTextBox";
            this.videoTextBox.Size = new System.Drawing.Size(120, 20);
            this.videoTextBox.TabIndex = 19;
            this.videoTextBox.Text = "Видео";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(115, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Видео";
            // 
            // savingsTextBox
            // 
            this.savingsTextBox.CausesValidation = false;
            this.savingsTextBox.Location = new System.Drawing.Point(83, 94);
            this.savingsTextBox.Name = "savingsTextBox";
            this.savingsTextBox.Size = new System.Drawing.Size(196, 20);
            this.savingsTextBox.TabIndex = 21;
            this.savingsTextBox.Text = "M:\\mail.cloud\\Хранилка";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Хранилка";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 198);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(351, 65);
            this.label10.TabIndex = 22;
            this.label10.Text = resources.GetString("label10.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 123);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Лог";
            // 
            // logTextBox
            // 
            this.logTextBox.Location = new System.Drawing.Point(83, 120);
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.Size = new System.Drawing.Size(196, 20);
            this.logTextBox.TabIndex = 24;
            this.logTextBox.Text = "M:\\mail.cloud\\Memory\\log.txt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(306, 96);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Найдено";
            // 
            // FilesCopiedNameLabel
            // 
            this.FilesCopiedNameLabel.AutoSize = true;
            this.FilesCopiedNameLabel.Location = new System.Drawing.Point(306, 118);
            this.FilesCopiedNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FilesCopiedNameLabel.Name = "FilesCopiedNameLabel";
            this.FilesCopiedNameLabel.Size = new System.Drawing.Size(74, 13);
            this.FilesCopiedNameLabel.TabIndex = 26;
            this.FilesCopiedNameLabel.Text = "Скопировано";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(306, 140);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Ошибок обработки";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(305, 162);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Время с начала:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(472, 83);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Время до завершения";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(471, 103);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "Время итого";
            // 
            // FindedBySearchLabel
            // 
            this.FindedBySearchLabel.AutoSize = true;
            this.FindedBySearchLabel.Location = new System.Drawing.Point(430, 96);
            this.FindedBySearchLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FindedBySearchLabel.Name = "FindedBySearchLabel";
            this.FindedBySearchLabel.Size = new System.Drawing.Size(13, 13);
            this.FindedBySearchLabel.TabIndex = 31;
            this.FindedBySearchLabel.Text = "0";
            // 
            // FilesCopiedLabel
            // 
            this.FilesCopiedLabel.AutoSize = true;
            this.FilesCopiedLabel.Location = new System.Drawing.Point(430, 118);
            this.FilesCopiedLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FilesCopiedLabel.Name = "FilesCopiedLabel";
            this.FilesCopiedLabel.Size = new System.Drawing.Size(13, 13);
            this.FilesCopiedLabel.TabIndex = 32;
            this.FilesCopiedLabel.Text = "0";
            // 
            // countErrorsLabel
            // 
            this.countErrorsLabel.AutoSize = true;
            this.countErrorsLabel.Location = new System.Drawing.Point(430, 140);
            this.countErrorsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.countErrorsLabel.Name = "countErrorsLabel";
            this.countErrorsLabel.Size = new System.Drawing.Size(13, 13);
            this.countErrorsLabel.TabIndex = 33;
            this.countErrorsLabel.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(472, 143);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(158, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Скорость сравнения Мбайт/с";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(649, 143);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 35;
            this.label18.Text = "0";
            // 
            // TimeFromStartLabel
            // 
            this.TimeFromStartLabel.AutoSize = true;
            this.TimeFromStartLabel.Location = new System.Drawing.Point(430, 162);
            this.TimeFromStartLabel.Name = "TimeFromStartLabel";
            this.TimeFromStartLabel.Size = new System.Drawing.Size(13, 13);
            this.TimeFromStartLabel.TabIndex = 36;
            this.TimeFromStartLabel.Text = "0";
            // 
            // loggerCheckBox
            // 
            this.loggerCheckBox.AutoSize = true;
            this.loggerCheckBox.Checked = true;
            this.loggerCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.loggerCheckBox.Location = new System.Drawing.Point(62, 123);
            this.loggerCheckBox.Name = "loggerCheckBox";
            this.loggerCheckBox.Size = new System.Drawing.Size(15, 14);
            this.loggerCheckBox.TabIndex = 37;
            this.loggerCheckBox.UseVisualStyleBackColor = true;
            this.loggerCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(14, 19);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "База";
            // 
            // mainFolderTextBox
            // 
            this.mainFolderTextBox.Location = new System.Drawing.Point(50, 16);
            this.mainFolderTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.mainFolderTextBox.Name = "mainFolderTextBox";
            this.mainFolderTextBox.Size = new System.Drawing.Size(229, 20);
            this.mainFolderTextBox.TabIndex = 39;
            this.mainFolderTextBox.Text = "M:\\mail.cloud\\Memory";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(26, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(60, 13);
            this.label20.TabIndex = 40;
            this.label20.Text = "Подпапки:";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(474, 35);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(123, 17);
            this.radioButton1.TabIndex = 42;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Мои воспоминания";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(474, 57);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(64, 17);
            this.radioButton2.TabIndex = 43;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Рандом";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(402, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Найдено в Memory";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(402, 211);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 13);
            this.label12.TabIndex = 45;
            this.label12.Text = "Найдено в Видео";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(402, 224);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "Найдено в Особых";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(402, 237);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(112, 13);
            this.label22.TabIndex = 47;
            this.label22.Text = "Найдено в Хранилке";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(568, 198);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(113, 13);
            this.label23.TabIndex = 48;
            this.label23.Text = "Добавлено в Memory";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(568, 237);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(124, 13);
            this.label24.TabIndex = 49;
            this.label24.Text = "Добавлено в Хранилку";
            // 
            // FindedInMemoryLabel
            // 
            this.FindedInMemoryLabel.AutoSize = true;
            this.FindedInMemoryLabel.Location = new System.Drawing.Point(529, 198);
            this.FindedInMemoryLabel.Name = "FindedInMemoryLabel";
            this.FindedInMemoryLabel.Size = new System.Drawing.Size(13, 13);
            this.FindedInMemoryLabel.TabIndex = 50;
            this.FindedInMemoryLabel.Text = "0";
            // 
            // FindedInVideoLabel
            // 
            this.FindedInVideoLabel.AutoSize = true;
            this.FindedInVideoLabel.Location = new System.Drawing.Point(529, 211);
            this.FindedInVideoLabel.Name = "FindedInVideoLabel";
            this.FindedInVideoLabel.Size = new System.Drawing.Size(13, 13);
            this.FindedInVideoLabel.TabIndex = 51;
            this.FindedInVideoLabel.Text = "0";
            // 
            // FindedInSpecialLabel
            // 
            this.FindedInSpecialLabel.AutoSize = true;
            this.FindedInSpecialLabel.Location = new System.Drawing.Point(529, 224);
            this.FindedInSpecialLabel.Name = "FindedInSpecialLabel";
            this.FindedInSpecialLabel.Size = new System.Drawing.Size(13, 13);
            this.FindedInSpecialLabel.TabIndex = 52;
            this.FindedInSpecialLabel.Text = "0";
            // 
            // FindedInSavingsLabel
            // 
            this.FindedInSavingsLabel.AutoSize = true;
            this.FindedInSavingsLabel.Location = new System.Drawing.Point(529, 237);
            this.FindedInSavingsLabel.Name = "FindedInSavingsLabel";
            this.FindedInSavingsLabel.Size = new System.Drawing.Size(13, 13);
            this.FindedInSavingsLabel.TabIndex = 53;
            this.FindedInSavingsLabel.Text = "0";
            // 
            // AddedInMemoryLabel
            // 
            this.AddedInMemoryLabel.AutoSize = true;
            this.AddedInMemoryLabel.Location = new System.Drawing.Point(708, 198);
            this.AddedInMemoryLabel.Name = "AddedInMemoryLabel";
            this.AddedInMemoryLabel.Size = new System.Drawing.Size(13, 13);
            this.AddedInMemoryLabel.TabIndex = 54;
            this.AddedInMemoryLabel.Text = "0";
            // 
            // AddedInSavingsLabel
            // 
            this.AddedInSavingsLabel.AutoSize = true;
            this.AddedInSavingsLabel.Location = new System.Drawing.Point(708, 237);
            this.AddedInSavingsLabel.Name = "AddedInSavingsLabel";
            this.AddedInSavingsLabel.Size = new System.Drawing.Size(13, 13);
            this.AddedInSavingsLabel.TabIndex = 55;
            this.AddedInSavingsLabel.Text = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.specialTextBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.videoTextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.savingsTextBox);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.logTextBox);
            this.groupBox1.Controls.Add(this.loggerCheckBox);
            this.groupBox1.Controls.Add(this.mainFolderTextBox);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Location = new System.Drawing.Point(15, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(287, 147);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Куда";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 276);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(739, 23);
            this.progressBar1.TabIndex = 57;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(676, 48);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 58;
            this.button2.Text = "Тест производительности";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 305);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.AddedInSavingsLabel);
            this.Controls.Add(this.AddedInMemoryLabel);
            this.Controls.Add(this.FindedInSavingsLabel);
            this.Controls.Add(this.FindedInSpecialLabel);
            this.Controls.Add(this.FindedInVideoLabel);
            this.Controls.Add(this.FindedInMemoryLabel);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.TimeFromStartLabel);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.countErrorsLabel);
            this.Controls.Add(this.FilesCopiedLabel);
            this.Controls.Add(this.FindedBySearchLabel);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.FilesCopiedNameLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.DeleteCheckBox);
            this.Controls.Add(this.FilesLoadedLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.FilesIncomeLabel);
            this.Controls.Add(this.FilesProcessedLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SubfoldersCheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.sourceTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox sourceTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox SubfoldersCheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label FilesProcessedLabel;
        private System.Windows.Forms.Label FilesIncomeLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label FilesLoadedLabel;
        private System.Windows.Forms.CheckBox DeleteCheckBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox specialTextBox;
        private System.Windows.Forms.TextBox videoTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox savingsTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label FilesCopiedNameLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label FindedBySearchLabel;
        private System.Windows.Forms.Label FilesCopiedLabel;
        private System.Windows.Forms.Label countErrorsLabel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label TimeFromStartLabel;
        private System.Windows.Forms.CheckBox loggerCheckBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox mainFolderTextBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label FindedInMemoryLabel;
        private System.Windows.Forms.Label FindedInVideoLabel;
        private System.Windows.Forms.Label FindedInSpecialLabel;
        private System.Windows.Forms.Label FindedInSavingsLabel;
        private System.Windows.Forms.Label AddedInMemoryLabel;
        private System.Windows.Forms.Label AddedInSavingsLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button button2;
    }
}

